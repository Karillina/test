$(document).ready(function() {
    $("form").submit(
        function(){
            sendAjaxForm('result_form', 'ajax_form', 'https://60376bfd5435040017722533.mockapi.io/form');
            return(false);
            }
    );
});
 
function sendAjaxForm(result_form, ajax_form, url) {
    $.ajax({
        url:     url,
        type:     "POST",
        dataType: "html",
        data: $("#"+ajax_form).serialize(),
        success: function(response) { 
            result = $.parseJSON(response);
            $('#result_form').html('Данные отправлены');
        },
        error: function (jqXHR, exception) {
        var msg = '';
        if (jqXHR.status === 0) {
            msg = 'Нет связи.\n Проверьте подключение.';
        } else if (jqXHR.status == 404) {
            msg = 'Запрашиваемая страница не найдена. Ошибка 404';
        } else if (jqXHR.status == 500) {
            msg = 'Внутренняя ошибка сервера [500].';
        } else if (exception === 'timeout') {
            msg = 'Превышено время ожидания.';
        }  else {
            msg = 'Неизвестная ошибка.\n' + jqXHR.responseText;
        }
        $('#result_form').html(msg);
        }
    });
}
